import processing.dxf.*;

boolean record;

int cx = 0;
int cy = 0;
int numPoints[] ={20,6,10,15,20};
int layerHeight[] = {0,10,20,30,40};
int layerRad[] = {100,150,75,50,65};
float camx = 0;
float camy = 0;

//int last

void setup(){
  size(480,320,P3D);
  background(255);
  fill(0);
  //increment = 360/numPoints;
  //println(radians(TWO_PI/numPoints));
}

void draw(){
  background(255);
  camera(camx, camy, (height/2)/tan(PI/6), mouseX, mouseY, 0.0, -1.0, -1.0, 1.0);
  //camera(width/2, height/2, (height/2)/tan(PI/6), width/2, height/2, 0.0, 1.0, 1.0, 1.0);
  pushMatrix();
  translate(width/2, height/2);
  //rotateX(5.0);
  
//********************************
// uncomment for DXF record
//********************************
 //if(record){
 // beginRaw(DXF,"model_output-" + currentTimeLabel() + ".dxf");
 //}
  
  for(int i = 0; i <layerHeight.length; i++){
    generateLayerPoints(numPoints[i],layerHeight[i],layerRad[i]);
    //generateLayerPoints(6,layerHeight);
  }

 //if(record){
 //  endRaw();
 //  record = false;
 //}
//********************************
  popMatrix();
}

void generateLayerPoints(int _numPoints, int _zPos, int _rad){
  float inc = 360/_numPoints;
  translate(0,0,_zPos);
  for(int i = 0; i< 360; i+=inc){
    float _x = _rad*cos(radians(i));
    float _y = _rad*sin(radians(i));
    //println(_x + "," + _y);
    ellipse(cx+_x,cy+_y,10,10); //<>//
    //increment = increment + TWO_PI/numPoints;
  }
}

String currentTimeLabel(){
  String ct = str(hour()) + str(minute()) + str(second());
  return ct;
}
  

void mouseDragged(){
  camx = mouseX;
  camy = mouseY;
  println(currentTimeLabel());
  //println(hour() + ":" + minute());
}